library(truncnorm)
#### Generate population and auxiliary variable

## Parameters
N <- 100
mu <- 1
sigma <- 1
sigma_u <- sqrt(0.01)
beta <- 1

xmin <- 0.1
xmax <- 2


epsilon <- rnorm(N, mu, sigma_u)
X <- rtruncnorm(N, xmin, xmax, 0, sigma_u) # Gaussian or High-tailed distribution (Pareto)
Y <- beta*X + epsilon